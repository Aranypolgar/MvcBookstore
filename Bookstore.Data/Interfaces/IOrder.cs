﻿using Bookstore.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Data.Interfaces
{
    public interface IOrder
    {
        void Create(Order order);
    }
}
