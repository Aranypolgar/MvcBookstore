﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bookstore.Data.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        
        public List<OrderDetail> OrderLines { get; set; }

        [Required]
        [Display(Name ="First name")]
        public string Firstname { get; set; }
        [Required]
        [Display(Name = "Last name")]
        public string Lastname { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Postcode { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
        [Required]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        public DateTime Created { get; set; }

    }
}
