﻿using Bookstore.Data.Database;
using Bookstore.Data.Interfaces;
using Bookstore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bookstore.Services
{
    public class OrderRepository : IOrder
    {
        private readonly BookstoreDbContext _ctx;
        private readonly ShoppingCart _cart;

        public OrderRepository(BookstoreDbContext ctx, ShoppingCart cart)
        {
            this._ctx = ctx;
            this._cart = cart;
        }

        public void Create(Order order)
        {
            order.Created = DateTime.Now;
            _ctx.Orders.Add(order);

            var items = _cart.ShoppingCartItems;

            foreach (var item in items)
            {
                var orderDetails = new OrderDetail()
                {
                    Quantity = item.Quantity,
                    BookId = item.Book.BookId,
                    OrderId = order.OrderId,
                    Price = item.Book.Price
                };

                _ctx.OrderDetails.Add(orderDetails);
            }
        }
    }
}
