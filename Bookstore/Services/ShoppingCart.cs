﻿using Bookstore.Data.Database;
using Bookstore.Data.Models;
using Bookstore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bookstore.Services
{
    public class ShoppingCart
    {
        private readonly BookstoreDbContext _context;

        public ShoppingCart(BookstoreDbContext context)
        {
            this._context = context;
        }

        public string ShoppingCartId { get; set; }

        public List<ShoppingCartItem> ShoppingCartItems { get; set; }

        // GET cart
        public static ShoppingCart GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;

            var context = services.GetService<BookstoreDbContext>();
            string cartId = session.GetString("CartId") ?? Guid.NewGuid().ToString();

            session.SetString("CartId", cartId);

            return new ShoppingCart(context) { ShoppingCartId = cartId };
        }

        public void AddToCart(Book book)
        {
            var item = _context
                .ShoppingCartItems
                .SingleOrDefault(s => s.Book.BookId == book.BookId 
                && s.ShoppingCartId == ShoppingCartId);

            if(item == null)
            {
                item = new ShoppingCartItem
                {
                    ShoppingCartId = ShoppingCartId,
                    Book = book,
                    Quantity = 1
                };

                _context.ShoppingCartItems.Add(item);
            }
            else
            {
                item.Quantity++;
            }

            _context.SaveChanges();
        }

        public int RemoveFromCart(Book book)
        {
            var item = _context
                .ShoppingCartItems
                .SingleOrDefault(s => s.Book.BookId == book.BookId
                && s.ShoppingCartId == ShoppingCartId);

            var quantity = 0;

            if(item != null)
            {
                if(item.Quantity >1)
                {
                    item.Quantity--;
                    quantity = item.Quantity;
                }
                else
                {
                    _context.ShoppingCartItems.Remove(item);                    
                }
            }

            _context.SaveChanges();
            return quantity;

        }

        public List<ShoppingCartItem> GetShoppingCartItems()
        {
            return ShoppingCartItems ?? 
                (ShoppingCartItems = _context
                .ShoppingCartItems
                .Where(c => c.ShoppingCartId == ShoppingCartId)
                .Include(s => s.Book).ToList());
        } 

        public void ClearCart()
        {
            var items = _context
                .ShoppingCartItems
                .Where(cart => cart.ShoppingCartId == ShoppingCartId);

            _context.ShoppingCartItems.RemoveRange(items);

            _context.SaveChanges();
        }

        public decimal GetTotal()
        {
            var total = _context
                .ShoppingCartItems
                .Where(c => c.ShoppingCartId == ShoppingCartId)
                .Select(c => c.Book.Price * c.Quantity)
                .Sum();

            return total;
        }
    }
}
